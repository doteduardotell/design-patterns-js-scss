# README #

Este README documentará todas as etapas necessárias para colocar o projeto em funcionamento.

### Para que serve este repositório? ###

* Para termos um padrão de:
	* Estrutura para desenvolvimento utilizando SASS
  	* Estrutura para desenvolvimento utilizando JavaScript
	* Arquivo GULP para geração de arquivos para produção

### Quais os pré-requisitos? ###

* Versão localmente de gulp em 4.0.2 ou superior
* Versão localmente de npm em 6.4.1

### Como faço para configurar? ###

* Clone o resposiório
* Caminhe até o respositorio escolhido raíz, rode "npm install --save-dev"
* Assim que a instalação for concluída, rode "gulp serve"

### Com quem posso falar? ###

* Eduardo Tell
* Marcos Printes