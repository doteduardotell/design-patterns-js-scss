var formulario = {
	inicia: function() {
		formulario.select.select2()
	},

	/**
	 * Aplica icone ao select2
	 * Para funcionar, é preciso:
	 * - Ter slick slide integrado.
	 *
	 * @return	void
	 * @author	Equipe SW
 	*/

	select: {
		select2: () => {
			
			if (!$(".select").length) return false 
			console.log("Inicia select2()");

			$('.select').select2();
		},
	},
}