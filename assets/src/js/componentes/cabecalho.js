var layout = {
	inicia: function() {
		layout.layout.cabecalho()
		layout.layout.rodape()
	},

	/**
	 * Aplica funções ao cabeçalho e rodapé
	 * Para funcionar: 
	 * - É preciso ter os blocos de Cabeçalho e Rodapé
	 *
	 * @return	void
	 * @author	Equipe SW
 	*/

	layout: {
		cabecalho: () => {
			console.log("Inicia cabecalho()");
		},

		rodape: () => {
			console.log("Inicia rodape()");
		}
	},
}