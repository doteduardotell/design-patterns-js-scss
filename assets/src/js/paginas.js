var guia = {
	inicia: function() {
		guia.slide.slick()
	},

	/**
	 * Aplica slick slide
	 * Para funcionar:
	 * - É preciso ter slick slide integrado
	 * - É preciso haver uma classe ".slick"
	 *
	 * @return	void
	 * @author	Equipe SW
 	*/

	slide: {
		slick: () => {

			if (!$(".slick").length) return false 
			console.log("Inicia slick()");

			$(".slick").each(function(index, element) {

				var slidesToShow = $(element).data("mostrar")
				var controles = $(element).data("dots")
				var setas = $(element).data("setas")

				var options = {
					infinite: true,
					slidesToShow: slidesToShow ? slidesToShow : 1,
					slidesToScroll: 1,
					dots: controles ? controles : false,
					arrows: setas ? setas : false,
				}

				$(element).slick(options);
			});
		},
	},
}
