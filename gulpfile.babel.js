import gulp from 'gulp';
import sass from 'gulp-sass';
import babel from 'gulp-babel';
import concat from 'gulp-concat';
import browserSync from "browser-sync";

// Função sass
// - Obtem os arquivos de SASS
// - sass: Tranpila de .scss para css; minifica; exibe erro se ouver
// - concat: todos os arquivos em um unico
// - gulp.dest: destino do arquivo complicado
exports.sass = () => (
    gulp.src('./assets/src/scss/**/**')
    .pipe(sass({outputStyle: 'compressed', errLogToConsole: true }))
    .pipe(concat('styles.min.css'))
    .pipe(gulp.dest('./assets/dist/css'))
);

// Função scripts
// - obtem os arquivos de componentes, todos da raiz
// - concat: arquivos em um unico
// - babel: transfere es6 em vesão compativel; minifica; remove comentarios; adiciona 'stricts'
// - - para funcionar, ter o .babelrcna raiz
// - gulp.dest: destino do arquivo complicado
exports.scripts = () => (
	gulp.src(['./assets/src/js/componentes/**.js', './assets/src/js/*.js'])
	.pipe(concat('scripts.min.js'))
	.pipe(babel())
	.pipe(gulp.dest('./assets/dist/js'))
);

// Tarefa watch observa todos os eventos dos respectivos caminhos
gulp.task('watch', () => {
    gulp.watch('./assets/src/scss/**/**', gulp.series('sass'))
    gulp.watch('./assets/src/js/**/**', gulp.series('scripts'))
});

// Tarefa serve 
// - browserSync: cria um servidor local com pasta principal em "index:"
// - watch: observa todos os eventos dos respectivos caminhos
// - watch com .change: Reload na página se ouver mudança em todos os arquivos do sistema
gulp.task('serve', () => {
    browserSync.init({
        server: {
            baseDir: './',
            index: 'index.html'
        },
        notify: false,
        injectChanges: true
    });

    gulp.watch('./assets/src/scss/**/**', gulp.series('sass'))
    gulp.watch('./assets/src/js/**/**', gulp.series('scripts'))
    gulp.watch('./**/**/**/**').on('change', browserSync.reload);
});

// Tarega padrão
// - Ececutada ao rodar "gulp"
gulp.task('default', gulp.series('watch'));